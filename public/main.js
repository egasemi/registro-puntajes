const app = {
    data() {
        return {
            jugadoras: [],
            nuevaJugadora: '',
            puntaje: '',
            indice: ''
        }
    },
    methods: {
        agregarJugadora: function() {
            const match = this.jugadoras.find(jugadora => jugadora.nombre === this.nuevaJugadora)
            if (match === undefined) {

                this.jugadoras.push({
                    nombre: this.nuevaJugadora,
                    puntaje: this.puntaje
                });

            } else {
                var nuevo_puntaje = parseInt(this.jugadoras[this.jugadoras.indexOf(match)].puntaje) + parseInt(this.puntaje)
                this.jugadoras[this.jugadoras.indexOf(match)].puntaje = nuevo_puntaje
                

            }

            this.jugadoras = this.jugadoras.sort((a, b) => b.puntaje - a.puntaje)
            localStorage.setItem('uno-jugadoras',JSON.stringify(this.jugadoras))
            this.nuevaJugadora = '',
            this.puntaje = '',
            this.indice = ''
        },

        eliminarJugadora: function(index) {
            this.jugadoras.splice(index,1);
            this.jugadoras = this.jugadoras.sort((a, b) => b.puntaje - a.puntaje)
            localStorage.setItem('uno-jugadoras',JSON.stringify(this.jugadoras))


        },
        registrarPuntaje: function(index) {
            this.nuevaJugadora  = this.jugadoras[index].nombre
            //this.puntaje = this.jugadoras[index].puntaje
            this.indice = index
        },
        limpiar: function() {
            localStorage.clear()
            this.jugadoras = []
            this.puntaje = ''
            this.nuevaJugadora = ''
        }
    },
    created: function() {
        let datosDB = JSON.parse(localStorage.getItem('uno-jugadoras'));
        if (datosDB === null) {
            this.jugadoras = []
        } else {
            this.jugadoras = datosDB
        }
    }
}

Vue.createApp(app).mount('#app')